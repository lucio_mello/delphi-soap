// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://www.camara.gov.br/SitCamaraWS/Deputados.asmx?wsdl
//  >Import : http://www.camara.gov.br/SitCamaraWS/Deputados.asmx?wsdl>0
// Encoding : utf-8
// Version  : 1.0
// (18/03/2021 08:48:45 - - $Rev: 96726 $)
// ************************************************************************ //

unit Deputados;

interface

uses Soap.InvokeRegistry, Soap.SOAPHTTPClient, System.Types, Soap.XSBuiltIns;

const
  IS_OPTN = $0001;
  IS_REF  = $0080;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Embarcadero types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:                - "https://www.camara.gov.br/SitCamaraWS/Deputados"[]
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]
  // !:schema          - "http://www.w3.org/2001/XMLSchema"[Gbl]


  ObterDetalhesDeputadoResult = TXMLData;      { "https://www.camara.gov.br/SitCamaraWS/Deputados"[CplxMxd] }
  ObterDeputadosResult = TXMLData;      { "https://www.camara.gov.br/SitCamaraWS/Deputados"[CplxMxd] }
  ObterPartidosCDResult = TXMLData;      { "https://www.camara.gov.br/SitCamaraWS/Deputados"[CplxMxd] }
  ObterIdCadastroOrcamentoResult = TXMLData;      { "https://www.camara.gov.br/SitCamaraWS/Deputados"[CplxMxd] }
  ObterLideresBancadasResult = TXMLData;      { "https://www.camara.gov.br/SitCamaraWS/Deputados"[CplxMxd] }
  ObterPartidosBlocoCDResult = TXMLData;      { "https://www.camara.gov.br/SitCamaraWS/Deputados"[CplxMxd] }

  // ************************************************************************ //
  // Namespace : https://www.camara.gov.br/SitCamaraWS/Deputados
  // soapAction: https://www.camara.gov.br/SitCamaraWS/Deputados/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // use       : literal
  // binding   : DeputadosSoap
  // service   : Deputados
  // port      : DeputadosSoap
  // URL       : https://www.camara.leg.br/SitCamaraWS/Deputados.asmx
  // ************************************************************************ //
  DeputadosSoap = interface(IInvokable)
  ['{C5E4B620-5358-FA96-2A17-B32A88929634}']
    function  ObterPartidosCD: ObterPartidosCDResult; stdcall;
    function  ObterPartidosBlocoCD(const idBloco: string; const numLegislatura: string): ObterPartidosBlocoCDResult; stdcall;
    function  ObterLideresBancadas: ObterLideresBancadasResult; stdcall;
    function  ObterIdCadastroOrcamento: ObterIdCadastroOrcamentoResult; stdcall;
    function  ObterDetalhesDeputado(const ideCadastro: string; const numLegislatura: string): ObterDetalhesDeputadoResult; stdcall;
    function  ObterDeputados: ObterDeputadosResult; stdcall;
  end;


  // ************************************************************************ //
  // Namespace : https://www.camara.gov.br/SitCamaraWS/Deputados
  // style     : ????
  // use       : ????
  // binding   : DeputadosHttpGet
  // service   : Deputados
  // port      : DeputadosHttpGet
  // ************************************************************************ //
  DeputadosHttpGet = interface(IInvokable)
  ['{8C609D4F-9375-D397-A1B6-7D5B947DD377}']
    function  ObterPartidosCD: Variant; stdcall;
    function  ObterPartidosBlocoCD(const idBloco: string; const numLegislatura: string): Variant; stdcall;
    function  ObterLideresBancadas: Variant; stdcall;
    function  ObterIdCadastroOrcamento: Variant; stdcall;
    function  ObterDetalhesDeputado(const ideCadastro: string; const numLegislatura: string): Variant; stdcall;
    function  ObterDeputados: Variant; stdcall;
  end;


  // ************************************************************************ //
  // Namespace : https://www.camara.gov.br/SitCamaraWS/Deputados
  // style     : ????
  // use       : ????
  // binding   : DeputadosHttpPost
  // service   : Deputados
  // port      : DeputadosHttpPost
  // ************************************************************************ //
  DeputadosHttpPost = interface(IInvokable)
  ['{7783F5BE-C875-DF99-6D97-C102041F64CB}']
    function  ObterPartidosCD: Variant; stdcall;
    function  ObterPartidosBlocoCD(const idBloco: string; const numLegislatura: string): Variant; stdcall;
    function  ObterLideresBancadas: Variant; stdcall;
    function  ObterIdCadastroOrcamento: Variant; stdcall;
    function  ObterDetalhesDeputado(const ideCadastro: string; const numLegislatura: string): Variant; stdcall;
    function  ObterDeputados: Variant; stdcall;
  end;

function GetDeputadosSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): DeputadosSoap;
function GetDeputadosHttpGet(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): DeputadosHttpGet;
function GetDeputadosHttpPost(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): DeputadosHttpPost;


implementation
  uses System.SysUtils;

function GetDeputadosSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): DeputadosSoap;
const
  defWSDL = 'http://www.camara.gov.br/SitCamaraWS/Deputados.asmx?wsdl';
  defURL  = 'https://www.camara.leg.br/SitCamaraWS/Deputados.asmx';
  defSvc  = 'Deputados';
  defPrt  = 'DeputadosSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as DeputadosSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


function GetDeputadosHttpGet(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): DeputadosHttpGet;
const
  defWSDL = 'http://www.camara.gov.br/SitCamaraWS/Deputados.asmx?wsdl';
  defURL  = '';
  defSvc  = 'Deputados';
  defPrt  = 'DeputadosHttpGet';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as DeputadosHttpGet);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


function GetDeputadosHttpPost(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): DeputadosHttpPost;
const
  defWSDL = 'http://www.camara.gov.br/SitCamaraWS/Deputados.asmx?wsdl';
  defURL  = '';
  defSvc  = 'Deputados';
  defPrt  = 'DeputadosHttpPost';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as DeputadosHttpPost);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  { DeputadosSoap }
  InvRegistry.RegisterInterface(TypeInfo(DeputadosSoap), 'https://www.camara.gov.br/SitCamaraWS/Deputados', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(DeputadosSoap), 'https://www.camara.gov.br/SitCamaraWS/Deputados/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(DeputadosSoap), ioDocument);
  { DeputadosSoap.ObterPartidosCD }
  InvRegistry.RegisterMethodInfo(TypeInfo(DeputadosSoap), 'ObterPartidosCD', '',
                                 '[ReturnName="ObterPartidosCDResult"]', IS_OPTN);
  { DeputadosSoap.ObterPartidosBlocoCD }
  InvRegistry.RegisterMethodInfo(TypeInfo(DeputadosSoap), 'ObterPartidosBlocoCD', '',
                                 '[ReturnName="ObterPartidosBlocoCDResult"]', IS_OPTN);
  { DeputadosSoap.ObterLideresBancadas }
  InvRegistry.RegisterMethodInfo(TypeInfo(DeputadosSoap), 'ObterLideresBancadas', '',
                                 '[ReturnName="ObterLideresBancadasResult"]', IS_OPTN);
  { DeputadosSoap.ObterIdCadastroOrcamento }
  InvRegistry.RegisterMethodInfo(TypeInfo(DeputadosSoap), 'ObterIdCadastroOrcamento', '',
                                 '[ReturnName="ObterIdCadastroOrcamentoResult"]', IS_OPTN);
  { DeputadosSoap.ObterDetalhesDeputado }
  InvRegistry.RegisterMethodInfo(TypeInfo(DeputadosSoap), 'ObterDetalhesDeputado', '',
                                 '[ReturnName="ObterDetalhesDeputadoResult"]', IS_OPTN);
  { DeputadosSoap.ObterDeputados }
  InvRegistry.RegisterMethodInfo(TypeInfo(DeputadosSoap), 'ObterDeputados', '',
                                 '[ReturnName="ObterDeputadosResult"]', IS_OPTN);
  { DeputadosHttpGet }
  InvRegistry.RegisterInterface(TypeInfo(DeputadosHttpGet), 'https://www.camara.gov.br/SitCamaraWS/Deputados', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(DeputadosHttpGet), '');
  { DeputadosHttpPost }
  InvRegistry.RegisterInterface(TypeInfo(DeputadosHttpPost), 'https://www.camara.gov.br/SitCamaraWS/Deputados', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(DeputadosHttpPost), '');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ObterDetalhesDeputadoResult), 'https://www.camara.gov.br/SitCamaraWS/Deputados', 'ObterDetalhesDeputadoResult');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ObterDeputadosResult), 'https://www.camara.gov.br/SitCamaraWS/Deputados', 'ObterDeputadosResult');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ObterPartidosCDResult), 'https://www.camara.gov.br/SitCamaraWS/Deputados', 'ObterPartidosCDResult');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ObterIdCadastroOrcamentoResult), 'https://www.camara.gov.br/SitCamaraWS/Deputados', 'ObterIdCadastroOrcamentoResult');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ObterLideresBancadasResult), 'https://www.camara.gov.br/SitCamaraWS/Deputados', 'ObterLideresBancadasResult');
  RemClassRegistry.RegisterXSInfo(TypeInfo(ObterPartidosBlocoCDResult), 'https://www.camara.gov.br/SitCamaraWS/Deputados', 'ObterPartidosBlocoCDResult');

end.