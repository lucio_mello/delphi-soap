object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'SOAP'
  ClientHeight = 409
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 23
    Width = 688
    Height = 41
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 8
    ExplicitTop = 50
    object Button1: TButton
      Left = 1
      Top = 1
      Width = 112
      Height = 39
      Align = alLeft
      Caption = 'Obter Deputados'
      TabOrder = 0
      OnClick = Button1Click
      ExplicitLeft = 9
      ExplicitTop = 7
      ExplicitHeight = 25
    end
    object Panel2: TPanel
      Left = 113
      Top = 1
      Width = 40
      Height = 39
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 153
      ExplicitTop = 0
      DesignSize = (
        40
        39)
      object ActivityIndicator1: TActivityIndicator
        Left = 3
        Top = 4
        Anchors = []
        IndicatorType = aitSectorRing
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 64
    Width = 688
    Height = 345
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    ExplicitLeft = 8
    ExplicitTop = 97
    ExplicitHeight = 368
    object TabSheet1: TTabSheet
      Caption = 'XML de resposta'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 0
      object Splitter1: TSplitter
        Left = 273
        Top = 0
        Width = 8
        Height = 317
        ExplicitHeight = 340
      end
      object Memo1: TMemo
        Left = 281
        Top = 0
        Width = 399
        Height = 317
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 0
        ExplicitLeft = 413
        ExplicitWidth = 264
        ExplicitHeight = 340
      end
      object TreeView1: TTreeView
        Left = 0
        Top = 0
        Width = 273
        Height = 317
        Align = alLeft
        Indent = 19
        TabOrder = 1
        ExplicitHeight = 340
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Tabela de dados'
      ImageIndex = 1
      ExplicitWidth = 677
      ExplicitHeight = 340
      object StringGrid1: TStringGrid
        Left = 0
        Top = 0
        Width = 680
        Height = 317
        Align = alClient
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
        TabOrder = 0
        ExplicitTop = 41
        ExplicitWidth = 688
        ExplicitHeight = 368
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 23
    Align = alTop
    Color = clSkyBlue
    ParentBackground = False
    TabOrder = 2
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 686
      Height = 21
      Align = alTop
      Alignment = taCenter
      Caption = 'Consumindo webservice com protocolo SOAP'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
    end
  end
  object HTTPRIO1: THTTPRIO
    WSDLLocation = 'http://www.camara.gov.br/SitCamaraWS/Deputados.asmx?wsdl'
    Service = 'Deputados'
    Port = 'DeputadosSoap'
    Converter.Options = [soSendMultiRefObj, soTryAllSchema, soRootRefNodesToBody, soCacheMimeResponse, soUTF8EncodeXML]
    Left = 632
    Top = 104
  end
end
