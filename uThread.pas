unit uThread;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Deputados, Soap.InvokeRegistry,
  System.Net.URLClient, Vcl.StdCtrls, Soap.Rio, Soap.SOAPHTTPClient,
  System.Types, Soap.XSBuiltIns, Vcl.ExtCtrls, Vcl.ComCtrls, XMLIntf,
  XMLDoc, Vcl.WinXCtrls, uSoap, ActiveX;

type
  TMinhaThread = class(TThread)
  protected
    procedure Execute; override;
  public
    constructor Create;
end;

implementation

constructor TMinhaThread.Create;
begin
   inherited Create(False);
end;

procedure TMinhaThread.Execute;
var
  oXml : IXMLDocument;
  res : ObterDeputadosResult;
  I, J: Integer;

  NodeText : string;
  NodeList : IXMLNodeList;
  NewItem: TTreeNode;
begin
  inherited;

  {XML Windows}
  CoInitialize(nil);

  {obt�m dados do webservice}
  res := (Form1.HTTPRIO1 As DeputadosSoap).ObterDeputados;

  {formata o XML}
  oXml := TXMLDocument.Create(nil);
  oXml.LoadFromXML(res.XMLNode.XML);
  oXml.XML.Text := xmlDoc.FormatXMLData(oXml.XML.Text);
  oXml.Active := true;

  {Preenche o treeview e o StringGrid}
  Form1.TreeView1.Items.Clear;
  Form1.StringGrid1.ColCount := 2;
  Form1.StringGrid1.RowCount := 2;
  for I := 0 to oXml.DocumentElement.ChildNodes.Count - 1 do
  begin
     NewItem := Form1.TreeView1.Items.Add(nil,'Deputado') ;
     Form1.StringGrid1.ColCount := oXml.DocumentElement.ChildNodes[I].ChildNodes.Count + 1;
     for J := 0 to oXml.DocumentElement.ChildNodes[I].ChildNodes.Count -1 do
     begin
        if oXml.DocumentElement.ChildNodes[I].ChildNodes[J].IsTextElement then
        begin
          Form1.TreeView1.Items.AddChild(NewItem,
            oXml.DocumentElement.ChildNodes[I].ChildNodes[J].Text);
          Form1.StringGrid1.Cells[0, i+1] := IntToStr(I);
          Form1.StringGrid1.Cells[j+1, i+1] := oXml.DocumentElement.ChildNodes[I].ChildNodes[J].Text;
        end;
     end;
     Form1.StringGrid1.RowCount := Form1.StringGrid1.RowCount + 1;
  end;

  Form1.ActivityIndicator1.Animate := False;

  {mostra no componente Memo}
  Form1.Memo1.Lines.Clear;
  Form1.Memo1.Lines.Add(oXml.Node.XML);
  Application.ProcessMessages;

end;



end.
